# Gojek App Cloning

Developed an end to end **Gojek App** using React Native with multiple screens


### Installation:
1. Development Platform
   * [Download Node JS](https://nodejs.org/en/) / [Download Yarn](https://yarnpkg.com/)
   * Android / IOS Simulator
2. Enter project directory 
3. Open the simulator (Android or Ios)
4. Open terminal and type `react-native run-ios` or `react-native run-android` 
5. Done! *If you wanna test, change the code in* **/src/components/pages/Home.js**

### Motivation:
 
*Plato once said, “Never discourage anyone who continually makes progress, no matter how slow.”*

